# README para la aplicación Users

## Introducción

Este README proporciona información sobre la aplicación Users, un pequeño servicio web creado en Java utilizando Spark, un framework web ligero. A continuación, se describen los pasos para configurar y ejecutar esta aplicación.

## Resumen de Características

- Proporciona una guía paso a paso para configurar y ejecutar la aplicación.
- Incluye ejemplos de rutas y acciones disponibles en la aplicación.
- Contiene información sobre cómo interactuar con la aplicación a través de solicitudes HTTP.

## Contenido

1. [¿Qué es esta aplicación?](#qué-es-esta-aplicación)
2. [¿Por qué deberías usar esta guía?](#por-qué-deberías-usar-esta-guía)
3. [Comenzando](#comenzando)
    - [Paso 1: Clonar el repositorio](#paso-1-clonar-el-repositorio)
    - [Paso 2: Compilar la aplicación](#paso-2-compilar-la-aplicación)
    - [Paso 3: Ejecutar la aplicación](#paso-3-ejecutar-la-aplicación)
4. [Rutas y Acciones](#rutas-y-acciones)
    - [POST /users](#post-users)
    - [GET /users/:id](#get-usersid)
    - [GET /users](#get-users)
    - [PUT /users/:id](#put-usersid)
    - [DELETE /users/:id](#delete-usersid)

## ¿Qué es esta aplicación?

La aplicación Users es un servicio web simple creado en Java utilizando Spark, un framework web ligero. Esta aplicación proporciona rutas para realizar operaciones CRUD básicas en recursos de usuarios. Puedes interactuar con la aplicación a través de solicitudes HTTP para crear, leer, actualizar y eliminar usuarios.

## ¿Por qué deberías usar esta guía?

Esta guía te ayudará a configurar y ejecutar la aplicación Users en tu entorno local. Si estás interesado en aprender cómo desarrollar servicios web en Java con Spark, esta guía te proporcionará los primeros pasos.

## Comenzando

A continuación, se describen los pasos para configurar y ejecutar la aplicación Users en tu máquina local.

### Paso 1: Clonar el repositorio

Para comenzar, clona el repositorio de la aplicación Users en tu máquina local utilizando Git:

```bash
git clone https://ruta-al-repositorio/users-app.git
```

### Paso 2: Compilar la aplicación

Una vez que hayas clonado el repositorio, accede al directorio de la aplicación:

```bash
cd users-app
```

Compila la aplicación utilizando el siguiente comando:

```bash
javac Users.java
```

### Paso 3: Ejecutar la aplicación

Una vez compilada la aplicación, puedes ejecutarla utilizando el siguiente comando:

```bash
java Users
```

La aplicación se ejecutará en el puerto 4000 de tu máquina local.

## Rutas y Acciones

A continuación, se describen las rutas disponibles en la aplicación Users y las acciones asociadas a cada una.

### POST /users

- Descripción: Crea un nuevo usuario.
- Ejemplo de solicitud HTTP:
  ```http
  POST http://localhost:4000/users
  ```
- Respuesta exitosa:
  ```plaintext
  Users post
  ```

### GET /users/:id

- Descripción: Obtiene información de un usuario específico por su ID.
- Ejemplo de solicitud HTTP:
  ```http
  GET http://localhost:4000/users/123
  ```
- Respuesta exitosa:
  ```plaintext
  Users get1
  ```

### GET /users

- Descripción: Obtiene una lista de todos los usuarios.
- Ejemplo de solicitud HTTP:
  ```http
  GET http://localhost:4000/users
  ```
- Respuesta exitosa:
  ```plaintext
  Users get2
  ```

### PUT /users/:id

- Descripción: Actualiza la información de un usuario existente por su ID.
- Ejemplo de solicitud HTTP:
  ```http
  PUT http://localhost:4000/users/123
  ```
- Respuesta exitosa:
  ```plaintext
  Users put
  ```

### DELETE /users/:id

- Descripción: Elimina un usuario existente por su ID.
- Ejemplo de solicitud HTTP:
  ```http
  DELETE http://localhost:4000/users/123
  ```
- Respuesta exitosa:
  ```plaintext
  Users delete
  ```

Esta guía te proporciona los pasos necesarios para configurar y ejecutar la aplicación Users. Puedes utilizar estas rutas y acciones para interactuar con la aplicación a través de solicitudes HTTP.
