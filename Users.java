import static spark.Spark.*;

public class Users{
    public static void main(String[] args){
        port(4000);
        post("/users",(req,res) -> "Users post");
        get("/users/:id",(req,res) -> "Users get1");
        get("/users",(req,res) -> "Users get2");
        put("/users/:id",(req,res) -> "Users put");
        path("/users/:id",(req,res) -> "Users path");
        delete("/users/:id",(req,res) -> "Users delete");
    }
}